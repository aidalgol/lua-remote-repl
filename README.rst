This module (``repl.lua``) provides a REPL on a socket, using `Lua coroutines`_.  It can be used with any event-driven program with an update/tick callback.  Depends upon `LuaSocket`_ and `LuaLogging`_.

In `LÖVE`_, add the following to main.lua::

  function love.load()
      require "repl"
  end

  function love.update()
      repl_tick()
  end

  function love.quit()
      repl_cleanup()
  end

.. _`Lua coroutines`: http://www.lua.org/manual/5.1/manual.html#2.11
.. _`LuaSocket`: http://w3.impa.br/~diego/software/luasocket/
.. _`LuaLogging`: http://keplerproject.org/lualogging/
.. _`LÖVE`: http://love2d.org/
