-- Lua remote REPL
-- Copyright (C) 2011  Kalio Ltda.
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its contributors
-- may be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

require "logging"
local logger = logging.new(function(self, level, message)
                             print(level .. ":", message)
                             return true
                           end)
logger:setLevel(logging.INFO)

local tcp_port = 9000
local tcp_bind_address = "localhost"
local unix_socket_name = "remote_lua_repl"

local use_tcp = false

local socket = require "socket"
if not use_tcp then
    socket.unix = require "socket.unix"
end

function repl_thread()
    logger:info("Entering repl_thread")

    local server,err = nil
    if use_tcp then
        server,err = socket.bind(tcp_bind_address, tcp_port)
    else
        -- assert because we should always be able to at least create a socket object.
        server = assert(socket.unix())
        fd,err = server:bind(unix_socket_name)
        server:listen()
    end
    if not server then
        logger:error(string.format("Bind failed with error: %s", err))
        return
    else
        logger:debug("Bind success")
    end

    repl_clients = {}
    if use_tcp then
        local ip, port = server:getsockname()
        local resolved_ip = socket.dns.toip(socket.dns.gethostname(tcp_bind_address))
        logger:info(string.format("Accepting connections at %s %s:%d",
                                  socket.dns.gethostname(tcp_bind_address),
                                  tostring(resolved_ip), port))
    else
        logger:info("Accepting connections on " .. unix_socket_name)
    end

    local client, err
    -- wait for a client to connect
    repeat
        server:settimeout(0.0001, 't')
        client, err = server:accept()

        if not client then
            coroutine.yield()
        else
            if use_tcp then
                logger:info(string.format("Client connected: %s:%s",
                                          client:getpeername()))
            else
                logger:info("Client connected")
            end
            handle_arriving_client(client)
        end

        local quit = false
        local error

        local active_clients = {}
        for client, client_t in pairs(repl_clients) do
            active_clients[#active_clients+1] = client
        end

        -- select on read sockets
        local rd, wr, error = socket.select(active_clients, nil, 0.0001)

        if not error then
            for i=1,#rd do
                local c = rd[i]
                coroutine.resume(repl_clients[c], c) -- coroutine, client
            end
        elseif error=="timeout" then
            coroutine.yield()
        end

    until false
end

function handle_arriving_client(client)
    if use_tcp then
        logger:info(string.format("Client arrived: %s", client:getpeername()))
    else
        logger:info("Client arrived")
    end
    local client_coroutine = coroutine.create(client_thread)
    repl_clients[client] = client_coroutine
    coroutine.resume(client_coroutine, client)
end

function handle_departing_client(client)
    if use_tcp then
        logger:info(string.format("Departing client: %s", client:getsockname()))
    else
        logger:info("Departing client")
    end
    repl_clients[client] = nil
end


function client_thread(client)
    -- REPL environment for the chunk
    local cenv = {}
    cenv['print']= (function (...)
                        _G.print(...)
                        local dots= {...}
                        for i=1,#dots do client:send(tostring(dots[i]).."\n") end
                    end)
    setmetatable(cenv, {__index = _G, __newindex= _G})


    client:send("Hello!\n")

    local error, quit
    local accLine= ""
    repeat
        client:send(get_prompt(accLine == "" and 1 or 2))
        coroutine.yield()
        local line, err = client:receive()
        local val = nil

        if not err
        then
            if line == "quit" and accLine == "" then
                quit = true
            else
                accLine= accLine.."\n"..line
                local res, lerr = loadstring(accLine)
                if res==nil then
                    if not string.find(lerr, "near '<eof>'$") then
                        -- it was a real syntax error and not just incomplete code
                        print("error: " .. lerr)
                        val = lerr
                        accLine = ""-- abort the whole chunk
                    end
                else
                    accLine= "" -- the whole accLine parsed as a chunk so cleanup
                    -- build an environment for the chunk.
                    -- print() will write to stdout on the host _and_ send the
                    -- string back through the socket

                    setfenv(res, cenv)

                    -- evaluate the chunk function
                    local ok, result = xpcall( res, function(err) return debug.traceback(err, 3)  end )

                    if ok then
                        val = result
                    else
                        logger:error(string.format("REPL chunk execution failed with error: %s",
                                                   tostring(result)))
                        val = result
                    end
                end
                client:send((val~=nil) and tostring(val).."\n" or "")
            end
        else
            logger:error(string.format("Socket error: %s", err))
            quit = true
        end
    until quit

    handle_departing_client(client)
    client:send("Bye\n")
    client:close()
    return
end

function get_prompt(level)
    local prompt
    if level == 1 then
        prompt = _PROMPT  or "> "
    else
        prompt = _PROMPT2 or ">> "
    end
    return prompt
end

logger:debug("About to create coroutine")
repl_coroutine = coroutine.create(repl_thread)

function repl_tick()
    if repl_coroutine and coroutine.status(repl_coroutine) == "dead" then
        logger:error("REPL may be unavailable")
        repl_coroutine = nil
    end
    if not repl_coroutine then
        return
    end

    local yielded, err = coroutine.resume(repl_coroutine)

    if yielded then
        logger:debug("Coroutine yielded")
    else
        logger:error(string.format("coroutine.resume failed with error: %s",
                                   err))
    end
end

function repl_cleanup()
    os.remove(unix_socket_name)
end
